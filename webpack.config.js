const lagertha = require('lagertha/webpack');
const merge   = require('webpack-merge');
const path    = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const PACKAGE = require('./package.json');
const TARGET  = process.env.npm_lifecycle_event;

const IS_DEVELOPMENT = (TARGET == 'start');

const PATHS = {
  app: path.resolve(__dirname, "app"),
  build: path.resolve(__dirname, "build"),
  build_dev: path.resolve(__dirname, "build_dev"),
  node_modules: path.join(__dirname, "node_modules")
};

const common = {
  entry: {
    main: 'main.js'
  },
  output: {
    path: PATHS.build,
    publicPath: '/',
    filename: '[name].bundle.js'
  },
  resolve: {
    extensions: ['', '.js'],
    fallback: [PATHS.app, PATHS.node_modules],
    modulesDirectories: ['node_modules', PATHS.app]
  },
  module: {
    loaders: [
      {test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, loader: 'file' },
      {test: /\.css$/, loader: "style-loader!css-loader"},
      {test: /\.jpg$/, loader: 'file-loader'},
      {test: /\.json$/, loader: 'json-loader'},
      {test: /\.png$/, exclude: /\.nop\.png$/, loader: "url-loader?limit=100000&mimetype=image/png"},
      {test: /\.tag$/, loader: 'tag?type=none'},
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: PACKAGE.name,
      template: 'app/template.html',
      inject: 'body',
      version: PACKAGE.version,
      buildDate: new Date().toUTCString()
    }),
    new webpack.DefinePlugin({
      __LAGERTHA_OPTIONS__: JSON.stringify(lagertha),
      DEVELOPMENT: IS_DEVELOPMENT,
      PRODUCTION: !IS_DEVELOPMENT,
      VERSION: JSON.stringify(PACKAGE.version)
    }),
    new webpack.BannerPlugin(PACKAGE.name + ' v' + PACKAGE.version + ' | build: ' + new Date().toUTCString() + ' |  (c) 2015 Viking Makt')
  ]
};

const develop = {
  devtool: 'eval-source-map',
  output: {
    path: PATHS.build_dev
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true,
    host: '0.0.0.0',
    port: process.env.PORT
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};

if (TARGET == 'start' || !TARGET) {
  module.exports = merge(common, develop);
}

if (TARGET == 'build') {
  module.exports = merge(common, {});
}
