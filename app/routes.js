var welcome = require('pages/welcome');
var router  = require('lagertha').router;

var HOMEPAGE = '.*';

router.on(HOMEPAGE, welcome.make);
