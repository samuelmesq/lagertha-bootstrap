require('./welcome.tag');
var mount = require('riot').mount;

var version = 'v'+VERSION;

function make() {
  return mount('#client-body', 'lagertha-welcome', { version: version });
}

module.exports = { make: make };
