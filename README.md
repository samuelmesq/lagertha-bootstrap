# Lagertha.js

web client framework

## Requirements

```
NODE v4.2.2
```

## Passo a passo

Clonar o repositório

```git clone git@gitlab.com:vikingmakt/lagertha-bootstrap.git```

Instalar as depêndencias

```npm i```

Executar na ENV de desenvolvimento na url http://localhost:8080 [1]

```npm start```

Executar Build

```npm run build```


----

[1] - por padrão a porta começa no 8080, e vai subindo até encontrar uma porta livre.
